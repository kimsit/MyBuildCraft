package com.lord.mybuildcraft;

import com.lord.mybuildcraft.client.gui.GuiFilter;
import com.lord.mybuildcraft.inventory.ContainerFilter;
import com.lord.mybuildcraft.tileentity.TileDiamondPipe;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class GuiHandler implements IGuiHandler {

    public static final int GUI_ROOTER_ID = 1;

    @Override
    public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case GUI_ROOTER_ID:
                TileDiamondPipe tile = (TileDiamondPipe) world.getTileEntity(new BlockPos(x, y, z));
                return new ContainerFilter(player.inventory, tile);

            default:
                return null;
        }
    }

    @Override
    public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
        switch (ID) {
            case GUI_ROOTER_ID:
                TileDiamondPipe tile = (TileDiamondPipe) world.getTileEntity(new BlockPos(x, y, z));
                return new GuiFilter(player.inventory, tile);

            default:
                return null;
        }
    }

}
