package com.lord.mybuildcraft.network;

import com.lord.mybuildcraft.TravelingItem;
import com.lord.mybuildcraft.tileentity.TilePipe;
import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class PipeUpdatePacket implements IMessage {

    public BlockPos pos;
    public TravelingItem item;
    private NBTTagCompound nbtSyncData;

    public PipeUpdatePacket() {
    }

    public PipeUpdatePacket(BlockPos pos, TravelingItem item) {
        this.pos = pos;
        this.item = item;
        this.nbtSyncData = new NBTTagCompound();
    }

    @Override
    public void fromBytes(ByteBuf buf) {
        int x = buf.readInt();
        int y = buf.readInt();
        int z = buf.readInt();
        pos = new BlockPos(x, y, z);
        nbtSyncData = ByteBufUtils.readTag(buf);

        item = TravelingItem.fromNBT(nbtSyncData);
    }

    @Override
    public void toBytes(ByteBuf buf) {
        item.writeToNBT(nbtSyncData);

        buf.writeInt(pos.getX());
        buf.writeInt(pos.getY());
        buf.writeInt(pos.getZ());
        ByteBufUtils.writeTag(buf, nbtSyncData);
    }

    public static class Handler implements IMessageHandler<PipeUpdatePacket, IMessage> {

        @Override
        public IMessage onMessage(final PipeUpdatePacket packet, MessageContext ctx) {
            if (ctx.side != Side.CLIENT) {
                System.out.println("WRONG SIDE !");
            }

            final WorldClient worldClient = Minecraft.getMinecraft().world;
            Minecraft.getMinecraft().addScheduledTask(new Runnable() {
                @Override
                public void run() {
                    processMessage(worldClient, packet.pos, packet.item);
                }
            });

            return null;
        }

        private void processMessage(WorldClient world, BlockPos pos, TravelingItem item) {
            if (!world.isBlockLoaded(pos)) {
                return;
            }

            TileEntity tileEntity = world.getTileEntity(pos);

            if (!(tileEntity instanceof TilePipe)) {
                System.out.println("Wrong tile type !");
                return;
            }

            TilePipe pipe = (TilePipe) tileEntity;
            pipe.handlePacket(item);
        }

    }

}
