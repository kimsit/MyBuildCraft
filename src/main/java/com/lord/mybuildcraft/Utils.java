package com.lord.mybuildcraft;

import com.lord.mybuildcraft.tileentity.IPipeEntry;
import com.lord.mybuildcraft.tileentity.TilePipe;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTBase;
import net.minecraft.nbt.NBTTagString;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.World;

import java.util.LinkedList;

public class Utils {

    // Item transfer utils

    /**
     * Try to output item into given tile, drop it otherwise
     */
    public static void outputItem(TravelingItem item, TileEntity tile, World world) {
        if (tile instanceof IPipeEntry) {
            ((IPipeEntry) tile).injectItem(item, item.output);
        } else if (tile instanceof IInventory) {
            Utils.addToInventory(item.getStack(), (IInventory) tile, true);
        } else {
            dropItem(world, item);
        }
    }

    /**
     * Add the given stack to random inventories around the given pos
     *
     * @return The amount of items that have been added
     */
    public static int addToRandomInventory(World world, BlockPos pos, ItemStack stack) {
        LinkedList<IInventory> inventories = new LinkedList<IInventory>();

        for (EnumFacing o : EnumFacing.VALUES) {
            TileEntity tile = world.getTileEntity(pos.offset(o));

            if (tile instanceof IInventory) {
                inventories.add((IInventory) tile);
            }
        }

        int totalAdded = 0;

        while (!inventories.isEmpty() && stack.getCount() > 0) {
            IInventory inventory = inventories.get(world.rand.nextInt(inventories.size()));
            inventories.remove(inventory);

            int added = addToInventory(stack, inventory, true);
//			stack.stackSize -= added;
            totalAdded += added;
        }

        return totalAdded;
    }

    /**
     * Add the given stack to the inventory, by trying to merge it into existing stacks before filling empty slots
     *
     * @return The amount of items that have been added to the inventory
     */
    public static int addToInventory(ItemStack stack, IInventory inventory, boolean doAdd) {
        if (stack == null || inventory == null || stack.getItem() == null) {
            System.out.println("Invalid argument to addItemToInventory");
            return 0;
        }

        int toAdd = stack.getCount(), added = 0;

        // Try to merge into existing stack
        for (int i = 0; i < inventory.getSizeInventory() && toAdd > 0; i++) {
            ItemStack stackInSlot = inventory.getStackInSlot(i);

            if (stackInSlot != ItemStack.EMPTY && stackInSlot.getItem() == stack.getItem() && stackInSlot.getMetadata() == stack.getMetadata()) {
                int transaction = Math.min(toAdd, inventory.getInventoryStackLimit() - stackInSlot.getCount());
                if (doAdd) {
                    stackInSlot.grow(transaction);
                }
                toAdd -= transaction;
                added += transaction;
            }
        }

        for (int i = 0; i < inventory.getSizeInventory() && toAdd > 0; i++) {
            if (inventory.getStackInSlot(i) == ItemStack.EMPTY) {
                int transaction = Math.min(toAdd, inventory.getInventoryStackLimit());
                if (doAdd) {
                    inventory.setInventorySlotContents(i, new ItemStack(stack.getItem(), transaction, stack.getMetadata()));
                }
                toAdd -= transaction;
                added += transaction;
            }
        }

        if (doAdd) {
            stack.shrink(added);
        }

        return added;
    }

    public static boolean addToRandomPipe(World world, BlockPos pos, ItemStack stack) {
        LinkedList<EnumFacing> choices = new LinkedList<EnumFacing>();

        for (EnumFacing o : EnumFacing.VALUES) {
            TileEntity tile = world.getTileEntity(pos.offset(o));

            if (tile instanceof TilePipe) {
                choices.add(o);
            }
        }

        if (!choices.isEmpty()) {
            EnumFacing orientation = choices.get(world.rand.nextInt(choices.size()));
            TilePipe pipe = (TilePipe) world.getTileEntity(pos.offset(orientation));
            pipe.injectItem(stack, orientation);
            return true;
        }

        return false;
    }

    public static void dropItem(World world, TravelingItem item) {
        if (!world.isRemote) {
            world.spawnEntity(item.toEntityItem());
        }
    }

    public static void throwItemAway(World world, BlockPos pos, ItemStack stack) {
        float f1 = world.rand.nextFloat() * 0.8F + 0.1F;
        float f2 = world.rand.nextFloat() * 0.8F + 0.6F;
        float f3 = world.rand.nextFloat() * 0.8F + 0.1F;

        EntityItem entityitem = new EntityItem(world, pos.getX() + f1, pos.getY() + f2, pos.getZ() + f3, stack);
        entityitem.setDefaultPickupDelay();

        entityitem.motionX = world.rand.nextGaussian() * 0.05F;
        entityitem.motionY = world.rand.nextGaussian() * 0.05F + 0.75F;
        entityitem.motionZ = world.rand.nextGaussian() * 0.05F;

        world.spawnEntity(entityitem);
    }

    // NBT utils

    public static NBTTagString NBTFromFacing(EnumFacing facing) {
        if (facing == null) {
            return new NBTTagString("NULL");
        }
        return new NBTTagString(facing.getName());
    }

    public static EnumFacing facingFromNBT(NBTBase nbt) {
        String value = ((NBTTagString) nbt).getString();
        if (value.equals("NULL")) {
            return null;
        }
        return EnumFacing.byName(value);
    }

    // Vec3 utils

    public static Vec3d vec3(double value) {
        return new Vec3d(value, value, value);
    }

    public static Vec3d convert(Vec3i vec3i) {
        return new Vec3d(vec3i.getX(), vec3i.getY(), vec3i.getZ());
    }

    public static Vec3d vecFromFacing(EnumFacing face) {
        return vecFromFacing(face, 1.0);
    }

    public static Vec3d vecFromFacing(EnumFacing face, double scale) {
        if (face == null) {
            return vec3(0.0);
        }
        return new Vec3d(scale * face.getFrontOffsetX(), scale * face.getFrontOffsetY(), scale * face.getFrontOffsetZ());
    }

    public static Vec3d convertMiddle(Vec3i vec3i) {
        return convert(vec3i).add(vec3(0.5));
    }

    public static Vec3d convertCentered(Vec3i vec3i) {
        return convert(vec3i).addVector(0.5, 0.25, 0.5);
    }
}
