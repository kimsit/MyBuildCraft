package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.Utils;
import com.lord.mybuildcraft.init.ModBlocks;
import net.minecraft.block.Block;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

public class TileMiningWell extends TileEntity {

    public boolean isWorking;

    private int depth;
    private long lastMining;

    /**
     * Number of ticks between updates
     */
    public static final int TICK_DELAY = 40;

    public TileMiningWell() {
        isWorking = true;
        depth = 0;
        lastMining = 0;
    }

    public void dig() {
        if (!isWorking || world.isRemote) {
            return;
        }

        if (getWorld().getWorldTime() - lastMining < TICK_DELAY) {
            return;
        }

        lastMining = getWorld().getWorldTime();
        depth++;

        while (getWorld().getBlockState(pos.offset(EnumFacing.DOWN, depth)).getBlock() == ModBlocks.plainPipeBlock) {
            depth++;
        }

        if (pos.getY() - depth < 0) {
            isWorking = false;
            return;
        }

        BlockPos targetPos = pos.offset(EnumFacing.DOWN, depth);
        IBlockState blockState = getWorld().getBlockState(targetPos);
        Block target = getWorld().getBlockState(targetPos).getBlock();

        if (target == Blocks.BEDROCK || target == Blocks.LAVA || target == Blocks.FLOWING_LAVA) {
            isWorking = false;
            return;
        }

        Item itemDropped = target.getItemDropped(blockState, getWorld().rand, 0);
        int quantity = target.quantityDropped(blockState, 0, getWorld().rand);

        if (itemDropped == null || quantity == 0) {
            return;
        }

        ItemStack stack = new ItemStack(itemDropped, quantity);
        getWorld().setBlockState(targetPos, ModBlocks.plainPipeBlock.getDefaultState());

        // Try to add to a nearby chest
        if (Utils.addToRandomInventory(getWorld(), pos, stack) > 0) {
            return;
        }

        // Try to add to a pipe
        if (Utils.addToRandomPipe(getWorld(), pos, stack)) {
            return;
        }

        // Throw the object away
        if (stack.getCount() > 0) {
            Utils.throwItemAway(getWorld(), pos, stack);
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        depth = compound.getInteger("depth");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        compound.setInteger("depth", depth);

        return compound;
    }

}
