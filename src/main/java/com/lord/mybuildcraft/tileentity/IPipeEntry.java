package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.TravelingItem;
import net.minecraft.util.EnumFacing;

public interface IPipeEntry {

    void injectItem(TravelingItem item, EnumFacing inputOrientation);

}
