package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.Utils;
import net.minecraft.block.Block;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.state.IBlockState;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;

public class TileMachine extends TileEntity implements ITickable {

    private int depth;
    private int step;
    private int timer;

    /**
     * Number of ticks between updates
     */
    public static final int TICK_DELAY = 20;

    public boolean isWorking;

    public static final int fieldSizeX = 3; // Size sideways
    public static final int fieldSizeZ = 3; // Size facing
    public static final int fieldSize = fieldSizeX * fieldSizeZ;

    public TileMachine() {
        depth = 0;
        step = 0;
        timer = TICK_DELAY;
        isWorking = true;
    }

    @Override
    public void update() {
        if (!isWorking || world.isRemote) {
            return;
        }

        if (timer > 0) {
            timer--;
            return;
        }
        timer = TICK_DELAY;

        int diffX = step % fieldSizeX;
        int diffZ = step / fieldSizeZ;

        EnumFacing front = getWorld().getBlockState(pos).getValue(BlockHorizontal.FACING).getOpposite();
        BlockPos targetPos = new BlockPos(this.pos);

        // Start in left corner
        targetPos = targetPos.down(depth).offset(front, 2).offset(front.rotateYCCW(), fieldSizeX / 2);

        // Offset to current step
        targetPos = targetPos.offset(front, diffZ).offset(front.rotateY(), diffX);

        step++;
        if (step >= fieldSize) {
            step = 0;
            depth++;
        }
        if (targetPos.getY() <= 0) {
            isWorking = false;
            return;
        }

        IBlockState state = getWorld().getBlockState(targetPos);
        Block target = state.getBlock();

        if (target == Blocks.AIR) {
            return;
        }

        if (target == Blocks.BEDROCK) {
            isWorking = false;
            return;
        }

        Item itemDropped = target.getItemDropped(state, getWorld().rand, 0);
        int quantity = target.quantityDropped(state, 0, getWorld().rand);

        if (itemDropped == null || quantity == 0) {
            return;
        }

        ItemStack stack = new ItemStack(itemDropped, quantity, target.damageDropped(state));
        getWorld().destroyBlock(targetPos, false);

//		System.out.println("Dropped stack: " + stack);

        // Try to add to a nearby chest
        Utils.addToRandomInventory(getWorld(), pos, stack);

        if (stack.getCount() == 0) {
            return;
        }

        // Try to add to a pipe
        if (Utils.addToRandomPipe(getWorld(), pos, stack)) {
            return;
        }

        // Throw the object away
        Utils.throwItemAway(getWorld(), pos, stack);
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        depth = compound.getInteger("depth");
        step = compound.getInteger("step");
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        compound.setInteger("depht", depth);
        compound.setInteger("step", step);

        return compound;
    }

}
