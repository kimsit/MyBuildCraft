package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.TravelingItem;
import com.lord.mybuildcraft.Utils;
import com.lord.mybuildcraft.inventory.ContainerFilter;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.util.Constants;
import net.minecraftforge.items.CapabilityItemHandler;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nullable;
import java.util.LinkedList;
import java.util.List;

public class TileDiamondPipe extends TilePipe implements ITickable, IPipeEntry {

    class ItemInTransit {
        TravelingItem item;
        long exitDate;
    }

    private ItemStackHandler filters;

    /**
     * Store each item in processing with its exit date
     */
    private LinkedList<ItemInTransit> items;

    public TileDiamondPipe() {
        items = new LinkedList<>();

        filters = new ItemStackHandler(ContainerFilter.SIZE) {
            @Override
            public int getSlotLimit(int slot) {
                return 1;
            }
        };
    }

    public ItemStackHandler getFilters() {
        return filters;
    }

    @Override
    public void injectItem(TravelingItem item, EnumFacing inputOrientation) {
        ItemInTransit data = new ItemInTransit();
        data.item = item;
        data.item.pos = Utils.convertCentered(this.pos);
        data.item.input = inputOrientation;
        data.exitDate = getWorld().getWorldTime() + 50;

        items.add(data);
    }

    @Override
    public void update() {
        if (world.isRemote) {
            return;
        }

        LinkedList<ItemInTransit> toRemove = new LinkedList<>();

        for (ItemInTransit data : items) {
            if (getWorld().getWorldTime() < data.exitDate) {
                continue;
            }

            EnumFacing out = resolveDestination(data);
            // Holy shit this is ugly
            data.item.output = out;

            System.out.println("Resolved dest for " + data.item.getStack() + " : " + out);

            toRemove.add(data);

            if (out == null) {
                Utils.throwItemAway(getWorld(), pos, data.item.getStack());
            } else {
                TileEntity tile = getWorld().getTileEntity(pos.offset(out));
                Utils.outputItem(data.item, tile, getWorld());
            }
        }

        items.removeAll(toRemove);
    }

    private EnumFacing resolveDestination(ItemInTransit data) {
        List<EnumFacing> filteredOutputs = new LinkedList<>();
        List<EnumFacing> leakOutputs = new LinkedList<>();
        ItemStack dataStack = data.item.getStack();

        // Rules : item allowed if corresponding filter is found or no filter at all is used
        for (EnumFacing dir : EnumFacing.VALUES) {
            // Don't bounce item back where it came from
            if (dir == data.item.input.getOpposite()) {
                continue;
            }

            boolean foundFilter = false;
            for (int slot = 0; slot < ContainerFilter.COLUMNS; slot++) {
                ItemStack stackInSlot = filters.getStackInSlot(dir.getIndex() * ContainerFilter.COLUMNS + slot);

                if (stackInSlot != ItemStack.EMPTY) {
                    foundFilter = true;

                    if (stackInSlot.getItem() == dataStack.getItem() && stackInSlot.getMetadata() == dataStack.getMetadata()) {
                        filteredOutputs.add(dir);
                        break;
                    }
                }
            }

            if (!foundFilter) {
                TileEntity tile = getWorld().getTileEntity(pos.offset(dir));

                if (tile instanceof IPipeEntry) {
                    leakOutputs.add(dir);
                }
            }
        }

        // Pick an output from filteredOutputs or leakOutputs if empty
        if (!filteredOutputs.isEmpty()) {
            return filteredOutputs.get(getWorld().rand.nextInt(filteredOutputs.size()));
        }

        if (!leakOutputs.isEmpty()) {
            return leakOutputs.get(getWorld().rand.nextInt(leakOutputs.size()));
        }

        // If both are empty, drop item
        return null;
    }

    // NBT

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        filters.deserializeNBT(compound.getCompoundTag("filters"));

        NBTTagList nbttaglist = compound.getTagList("items", Constants.NBT.TAG_COMPOUND);

        for (int i = 0; i < nbttaglist.tagCount(); i++) {
            NBTTagCompound tagItem = nbttaglist.getCompoundTagAt(i);
            ItemInTransit data = new ItemInTransit();
            data.item = TravelingItem.fromNBT(tagItem);
            data.exitDate = tagItem.getLong("exitDate");
            items.add(data);
        }

        super.readFromNBT(compound);
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        compound.setTag("filters", filters.serializeNBT());

        NBTTagList nbttaglist = new NBTTagList();

        for (ItemInTransit data : items) {
            NBTTagCompound tagItem = new NBTTagCompound();
            nbttaglist.appendTag(tagItem);
            data.item.writeToNBT(tagItem);
            tagItem.setLong("exitDate", data.exitDate);
        }

        compound.setTag("items", nbttaglist);

        return super.writeToNBT(compound);
    }

    // Capabilities

    @Override
    public boolean hasCapability(Capability<?> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY || super.hasCapability(capability, facing);
    }

    @Nullable
    @Override
    public <T> T getCapability(Capability<T> capability, @Nullable EnumFacing facing) {
        return capability == CapabilityItemHandler.ITEM_HANDLER_CAPABILITY ? (T)filters : super.getCapability(capability, facing);
    }

}
