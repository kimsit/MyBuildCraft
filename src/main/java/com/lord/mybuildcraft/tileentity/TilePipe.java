package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.Buildcraft;
import com.lord.mybuildcraft.TravelingItem;
import com.lord.mybuildcraft.Utils;
import com.lord.mybuildcraft.network.PipeUpdatePacket;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.Vec3d;
import net.minecraftforge.common.util.Constants;

import java.util.LinkedList;
import java.util.List;

public class TilePipe extends TileEntity implements ITickable, IPipeEntry {

    private List<TravelingItem> items;

    public TilePipe() {
        items = new LinkedList<>();
    }

    public void injectItem(ItemStack stack, EnumFacing direction) {
        Vec3d itemPos = Utils.convertCentered(pos).add(Utils.vecFromFacing(direction, -0.4));

        TravelingItem pipedItem = TravelingItem.make(itemPos, stack.copy());

        this.injectItem(pipedItem, direction);
    }

    @Override
    public void injectItem(TravelingItem item, EnumFacing inputOrientation) {
        item.input = inputOrientation;
        item.output = null;
        item.toCenter = true;

        if (!getWorld().isRemote) {
            item.output = resolveDestination(item);
            sendUpdatePacket(item);
//			System.out.println(item.getStack());
        }

        addItemToSet(item);
    }

    private void addItemToSet(TravelingItem item) {
        item.setContainer(this);
        items.add(item);
    }

    @Override
    public void update() {
        List<TravelingItem> itemsScheduledForRemoval = new LinkedList<>();

        for (TravelingItem item : items) {
            EnumFacing direction = item.toCenter ? item.input : item.output;
            item.move(Utils.vecFromFacing(direction, item.getSpeed()));

            if ((item.toCenter && middleReached(item)) || outOfBounds(item)) {
                item.toCenter = false;

                // Readjust pos to middle
                item.pos = Utils.convertCentered(pos);

                if (item.output == null) {
                    itemsScheduledForRemoval.add(item);
                    Utils.dropItem(getWorld(), item);
                }
            } else if (!item.toCenter && endReached(item)) {
                itemsScheduledForRemoval.add(item);

                if (item.output == null) {
                    System.out.println("NULL OUTPUT");
                    continue;
                }

                TileEntity tile = getWorld().getTileEntity(pos.offset(item.output));
                Utils.outputItem(item, tile, getWorld());
            }
        }

        items.removeAll(itemsScheduledForRemoval);
    }

    protected EnumFacing resolveDestination(TravelingItem item) {
        LinkedList<EnumFacing> possibleMovements = getPossibleMovements(item.input, item.getStack());

        if (possibleMovements.isEmpty()) {
            return null;
        }

        return possibleMovements.get(getWorld().rand.nextInt(possibleMovements.size()));
    }

    protected LinkedList<EnumFacing> getPossibleMovements(EnumFacing from, ItemStack stack) {
        LinkedList<EnumFacing> result = new LinkedList<>();

        for (EnumFacing orientation : EnumFacing.VALUES) {
            if (orientation != from.getOpposite()) {
                TileEntity tile = getWorld().getTileEntity(pos.offset(orientation));

                if (tile instanceof IPipeEntry) {
                    result.add(orientation);
                } else if (tile instanceof IInventory) {
                    if (Utils.addToInventory(stack, (IInventory) tile, false) > 0) {
                        result.add(orientation);
                    }
                }
            }
        }

        return result;
    }

//	@Override
//	public Packet getDescriptionPacket() {
//		NBTTagCompound nbt = new NBTTagCompound();
//		this.writeToNBT(nbt);	
//		S35PacketUpdateTileEntity packet = new S35PacketUpdateTileEntity(pos, 0, nbt);
//		return packet;
//	}
//
//	@Override
//	public void onDataPacket(NetworkManager net, S35PacketUpdateTileEntity pkt) {		
//		if (!worldObj.isRemote) {
//			return;
//		}
//		
//		NBTTagCompound nbt = pkt.getNbtCompound();
//		this.readFromNBT(nbt);
//	}

    private void sendUpdatePacket(TravelingItem item) {
        PipeUpdatePacket packet = new PipeUpdatePacket(pos, item);
        Buildcraft.network.sendToAll(packet);
    }

    public void handlePacket(TravelingItem item_data) {
        TravelingItem searchedItem = null;

        for (TravelingItem item : items) {
            if (item.getId() == item_data.getId()) {
//				System.out.println("Found corresponding item");
                searchedItem = item;
            }
        }

        if (searchedItem == null) {
            addItemToSet(item_data);
        } else {
            searchedItem.input = item_data.input;
            searchedItem.output = item_data.output;
            searchedItem.pos = item_data.pos;
            searchedItem.toCenter = item_data.toCenter;
            searchedItem.setSpeed(item_data.getSpeed());
            searchedItem.setStack(item_data.getStack().copy());
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        NBTTagList nbttaglist = compound.getTagList("travelingItems", Constants.NBT.TAG_COMPOUND);

        for (int i = 0; i < nbttaglist.tagCount(); i++) {
            NBTTagCompound data = nbttaglist.getCompoundTagAt(i);

            TravelingItem item = TravelingItem.fromNBT(data);

            addItemToSet(item);
//			sendUpdatePacket(item);
        }
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        NBTTagList nbttaglist = new NBTTagList();

        for (TravelingItem item : items) {
            NBTTagCompound data = new NBTTagCompound();
            nbttaglist.appendTag(data);
            item.writeToNBT(data);
        }

        compound.setTag("travelingItems", nbttaglist);

        return compound;
    }

    protected boolean middleReached(TravelingItem item) {
        return item.pos.distanceTo(Utils.convertCentered(pos)) < Math.abs(item.getSpeed() * 0.55);
    }

    protected boolean endReached(TravelingItem item) {
        return item.pos.distanceTo(Utils.convertCentered(pos)) > 0.4;
    }

    protected boolean outOfBounds(TravelingItem item) {
        return item.pos.distanceTo(Utils.convertCentered(pos)) > 1.0;
    }

    public List<TravelingItem> getItems() {
        return items;
    }

}
