package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.Utils;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.tileentity.TileEntityChest;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;

import java.util.ArrayList;
import java.util.List;

public class TileWoodenPipe extends TilePipe {

    private long lastActivation = 0;

    public void extract() {
        if (world.getWorldTime() - lastActivation < 50) {
            return;
        }

        lastActivation = world.getWorldTime();

        List<BlockPos> inventories = new ArrayList<>();

        for (EnumFacing o : EnumFacing.VALUES) {
            TileEntity tile = world.getTileEntity(pos.offset(o));

            if (tile instanceof TileEntityChest) {
                TileEntityChest chest = (TileEntityChest) tile;
                if (!chest.isEmpty()) {
                    inventories.add(chest.getPos());
                }
            }
        }

        if (inventories.isEmpty()) {
            return;
        }

        BlockPos chestPos = inventories.get(world.rand.nextInt(inventories.size()));
        TileEntityChest chest = (TileEntityChest) world.getTileEntity(chestPos);
        ItemStack extracted = ItemStack.EMPTY;

        for (int i = 0; i < chest.getSizeInventory(); i++) {
            ItemStack contents = chest.getStackInSlot(i);
            if (contents.getCount() > 0) {
                extracted = contents.copy();
                extracted.setCount(1);

                chest.decrStackSize(i, 1);
                break;
            }
        }

        if (Utils.addToRandomPipe(world, pos, extracted)) {
            return;
        }

        Utils.throwItemAway(world, pos, extracted);
    }

}
