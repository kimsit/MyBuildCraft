package com.lord.mybuildcraft.tileentity;

import com.lord.mybuildcraft.Utils;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.EnumFacing;

import java.util.LinkedList;

public class TileIronPipe extends TilePipe {

    public EnumFacing output;

    public TileIronPipe() {
        this.output = null;
    }

    @Override
    public LinkedList<EnumFacing> getPossibleMovements(EnumFacing from, ItemStack stack) {
        LinkedList<EnumFacing> destinations = new LinkedList<EnumFacing>();
        destinations.add(output);
        return destinations;
    }

    public void cycleOrientation() {
        int index = output != null ? output.getIndex() + 1 : 0;

        for (int i = 0; i < EnumFacing.VALUES.length; i++) {
            EnumFacing dir = EnumFacing.getFront(index + i);

            if (getWorld().getTileEntity(pos.offset(dir)) instanceof IPipeEntry) {
                output = dir;
                return;
            }
        }
    }

    @Override
    public void readFromNBT(NBTTagCompound compound) {
        super.readFromNBT(compound);

        output = Utils.facingFromNBT(compound.getTag("output"));
    }

    @Override
    public NBTTagCompound writeToNBT(NBTTagCompound compound) {
        super.writeToNBT(compound);

        compound.setTag("output", Utils.NBTFromFacing(output));

        return compound;
    }

}
