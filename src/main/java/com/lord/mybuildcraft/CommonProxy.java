package com.lord.mybuildcraft;

import com.lord.mybuildcraft.init.ModBlocks;
import com.lord.mybuildcraft.init.ModItems;
import com.lord.mybuildcraft.tileentity.*;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod.EventBusSubscriber
public class CommonProxy {

    public void preInit() {
        registerTileEntities();
    }

    public void init() {
        NetworkRegistry.INSTANCE.registerGuiHandler(Buildcraft.INSTANCE, new GuiHandler());
    }

    public void postInit() {

    }

    @SubscribeEvent
    public static void registerItems(RegistryEvent.Register<Item> event) {
        ModItems.registerItems(event.getRegistry());
        ModBlocks.registerItemBlocks(event.getRegistry());
    }

    @SubscribeEvent
    public static void registerBlocks(RegistryEvent.Register<Block> event) {
        ModBlocks.registerBlocks(event.getRegistry());
    }

    private static void registerTileEntities() {
        GameRegistry.registerTileEntity(TileMachine.class, "machine");
        GameRegistry.registerTileEntity(TileMiningWell.class, "miningWell");
        GameRegistry.registerTileEntity(TileWoodenPipe.class, "woodenPipe");
        GameRegistry.registerTileEntity(TileStonePipe.class, "stonePipe");
        GameRegistry.registerTileEntity(TileIronPipe.class, "ironPipe");
        GameRegistry.registerTileEntity(TileGoldPipe.class, "goldPipe");
        GameRegistry.registerTileEntity(TileDiamondPipe.class, "diamondPipe");
    }
}
