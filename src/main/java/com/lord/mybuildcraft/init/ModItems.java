package com.lord.mybuildcraft.init;

import com.lord.mybuildcraft.item.BuildcraftItem;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class ModItems {

    public static Item woodenGearItem;
    public static Item ironGearItem;

    public static void registerItems(IForgeRegistry<Item> registry) {
        woodenGearItem = new BuildcraftItem("wooden_gear");
        ironGearItem = new BuildcraftItem("iron_gear");

        registry.registerAll(woodenGearItem, ironGearItem);
    }

    @SideOnly(Side.CLIENT)
    public static void initModels() {
        initItemModel(woodenGearItem);
        initItemModel(ironGearItem);
    }

    private static void initItemModel(Item item) {
        ModelLoader.setCustomModelResourceLocation(item, 0,
                new ModelResourceLocation(item.getRegistryName(), "inventory"));
    }
}
