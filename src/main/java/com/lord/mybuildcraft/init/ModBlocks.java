package com.lord.mybuildcraft.init;

import com.lord.mybuildcraft.block.*;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.registries.IForgeRegistry;

public class ModBlocks {

    public static BlockWoodenPipe woodenPipeBlock;
    public static BlockStonePipe stonePipeBlock;
    public static BlockIronPipe ironPipeBlock;
    public static BlockGoldPipe goldPipeBlock;
    public static BlockDiamondPipe diamondPipeBlock;
    public static BlockPlainPipe plainPipeBlock;

    public static BlockMachine machineBlock;
    public static BlockMiningWell miningWellBlock;

    public static void registerBlocks(IForgeRegistry<Block> registry) {
        stonePipeBlock = new BlockStonePipe("stone_pipe");
        woodenPipeBlock = new BlockWoodenPipe("wooden_pipe");
        ironPipeBlock = new BlockIronPipe("iron_pipe");
        goldPipeBlock = new BlockGoldPipe("gold_pipe");
        diamondPipeBlock = new BlockDiamondPipe("diamond_pipe");
        plainPipeBlock = new BlockPlainPipe("mining_pipe");

        machineBlock = new BlockMachine("quarry");
        miningWellBlock = new BlockMiningWell("mining_well");

        registry.registerAll(
                stonePipeBlock,
                woodenPipeBlock,
                ironPipeBlock,
                goldPipeBlock,
                diamondPipeBlock,
                plainPipeBlock,
                machineBlock,
                miningWellBlock
        );
    }

    public static void registerItemBlocks(IForgeRegistry<Item> registry) {
        registry.registerAll(
                stonePipeBlock.createItemBlock(),
                woodenPipeBlock.createItemBlock(),
                ironPipeBlock.createItemBlock(),
                goldPipeBlock.createItemBlock(),
                diamondPipeBlock.createItemBlock(),
                plainPipeBlock.createItemBlock(),
                machineBlock.createItemBlock(),
                miningWellBlock.createItemBlock()
        );
    }

    @SideOnly(Side.CLIENT)
    public static void initModels() {
        initBlockModel(stonePipeBlock);
        initBlockModel(woodenPipeBlock);
        initBlockModel(ironPipeBlock);
        initBlockModel(goldPipeBlock);
        initBlockModel(diamondPipeBlock);
        initBlockModel(plainPipeBlock);
        initBlockModel(miningWellBlock);
        initBlockModel(machineBlock);
    }

    private static void initBlockModel(Block block) {
        ModelLoader.setCustomModelResourceLocation(Item.getItemFromBlock(block), 0,
                new ModelResourceLocation(block.getRegistryName(), "inventory"));
    }

}
