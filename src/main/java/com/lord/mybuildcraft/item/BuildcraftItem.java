package com.lord.mybuildcraft.item;

import com.lord.mybuildcraft.Buildcraft;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;

public class BuildcraftItem extends Item {

    public BuildcraftItem(String name) {
        super();

        setRegistryName(Buildcraft.MODID, name);
        setUnlocalizedName(name);
        setCreativeTab(CreativeTabs.REDSTONE);
    }
}
