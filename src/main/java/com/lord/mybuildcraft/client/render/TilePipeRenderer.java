package com.lord.mybuildcraft.client.render;

import com.lord.mybuildcraft.TravelingItem;
import com.lord.mybuildcraft.Utils;
import com.lord.mybuildcraft.tileentity.TilePipe;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderEntityItem;
import net.minecraft.client.renderer.tileentity.TileEntitySpecialRenderer;
import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.Vec3d;
import org.lwjgl.opengl.GL11;

public class TilePipeRenderer extends TileEntitySpecialRenderer<TilePipe> {

    private static final EntityItem dummyEntityItem = new EntityItem(null);
    private static final RenderEntityItem customRenderItem;

    static {
        customRenderItem = new RenderEntityItem(Minecraft.getMinecraft().getRenderManager(),
                Minecraft.getMinecraft().getRenderItem()) {
            @Override
            public boolean shouldSpreadItems() {
                return false;
            }

            @Override
            public boolean shouldBob() {
                return false;
            }
        };
    }

    /**
     * render the tile entity - called every frame while the tileentity is in view of the player
     *
     * @param pipe         the associated tile entity
     * @param x            the x distance from the player's eye to the tileentity
     * @param y            the y distance from the player's eye to the tileentity
     * @param z            the z distance from the player's eye to the tileentity
     * @param partialTicks the fraction of a tick that this frame is being rendered at - used to interpolate frames between
     *                     ticks, to make animations smoother.  For example - if the frame rate is steady at 80 frames per second,
     *                     this method will be called four times per tick, with partialTicks spaced 0.25 apart, (eg) 0, 0.25, 0.5, 0.75
     * @param destroyStage the progress of the block being damaged (0 - 10), if relevant.  -1 if not relevant.
     */
    @Override
    public void render(TilePipe pipe, double x, double y, double z, float partialTicks, int destroyStage, float alpha) {
        if (pipe == null) return;

        for (TravelingItem item : pipe.getItems()) {
            EnumFacing direction = item.toCenter ? item.input : item.output;
            Vec3d motion = Utils.vecFromFacing(direction, item.getSpeed() * partialTicks);

            renderItem(item, x + item.pos.x + motion.x - pipe.getPos().getX(),
                    y + item.pos.y + motion.y - pipe.getPos().getY(),
                    z + item.pos.z + motion.z - pipe.getPos().getZ());

//			renderItem(item, x + item.pos.xCoord - pipe.getPos().getX(),
//					y + item.pos.yCoord - pipe.getPos().getY(),
//					z + item.pos.zCoord - pipe.getPos().getZ());
        }
    }

    public static void renderItem(TravelingItem item, double x, double y, double z) {
        float renderScale = 0.9F;
        ItemStack stack = item.getStack();

        try {
            GL11.glPushMatrix();
            GL11.glPushAttrib(GL11.GL_ENABLE_BIT);

            // First we need to set up the translation so that we render our gem with the bottom point at 0,0,0
            // when the renderTileEntityAt method is called, the tessellator is set up so that drawing a dot at [0,0,0] corresponds to the player's eyes
            // This means that, in order to draw a dot at the TileEntity [x,y,z], we need to translate the reference frame by the difference between the
            // two points, i.e. by the [relativeX, relativeY, relativeZ] passed to the method.  If you then draw a cube from [0,0,0] to [1,1,1], it will
            // render exactly over the top of the TileEntity's block.
            // In this example, the zero point of our model needs to be in the middle of the block, not at the [x,y,z] of the block, so we need to
            // add an extra offset as well
            GlStateManager.translate(x, y, z);

            GlStateManager.scale(renderScale, renderScale, renderScale);

            dummyEntityItem.setItem(stack);
            customRenderItem.doRender(dummyEntityItem, 0, 0, 0, 0, 0);
        } finally {
            GL11.glPopAttrib();
            GL11.glPopMatrix();
        }
    }

}
