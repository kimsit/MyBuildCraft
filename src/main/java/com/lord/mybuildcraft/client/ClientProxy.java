package com.lord.mybuildcraft.client;

import com.lord.mybuildcraft.CommonProxy;
import com.lord.mybuildcraft.client.render.TilePipeRenderer;
import com.lord.mybuildcraft.init.ModBlocks;
import com.lord.mybuildcraft.init.ModItems;
import com.lord.mybuildcraft.tileentity.TilePipe;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.fml.client.registry.ClientRegistry;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy {

    @Override
    public void init() {
        super.init();

        ClientRegistry.bindTileEntitySpecialRenderer(TilePipe.class, new TilePipeRenderer());
    }

    @SubscribeEvent
    public static void registerModels(ModelRegistryEvent event) {
        ModBlocks.initModels();
        ModItems.initModels();
    }

}
