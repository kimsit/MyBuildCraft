package com.lord.mybuildcraft.client.gui;

import com.lord.mybuildcraft.Buildcraft;
import com.lord.mybuildcraft.inventory.ContainerFilter;
import com.lord.mybuildcraft.tileentity.TileDiamondPipe;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.inventory.IInventory;
import net.minecraft.util.ResourceLocation;

public class GuiFilter extends GuiContainer {

    public GuiFilter(IInventory playerInventory, TileDiamondPipe tileDiamondPipe) {
        super(new ContainerFilter(playerInventory, tileDiamondPipe));

        this.xSize = 175;
        this.ySize = 225;
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        this.mc.getTextureManager().bindTexture(new ResourceLocation(Buildcraft.MODID, "textures/gui/filter.png"));
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);

        int j = (width - xSize) / 2;
        int k = (height - ySize) / 2;
        this.drawTexturedModalRect(j, k, 0, 0, xSize, ySize);
    }

}
