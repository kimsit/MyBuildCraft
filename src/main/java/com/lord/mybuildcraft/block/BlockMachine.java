package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.tileentity.TileMachine;
import net.minecraft.block.BlockHorizontal;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.init.Blocks;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockMachine extends BlockBuildcraft implements ITileEntityProvider {

    public BlockMachine(String name) {
        super(name, Material.IRON);
        setHardness(1.5f);
        setResistance(10f);
        setSoundType(SoundType.METAL);

        setDefaultState(this.blockState.getBaseState().withProperty(BlockHorizontal.FACING, EnumFacing.NORTH));
    }

    @Override
    public IBlockState getStateForPlacement(World world, BlockPos pos, EnumFacing facing, float hitX, float hitY, float hitZ,
                                            int meta, EntityLivingBase placer, EnumHand hand) {
        EnumFacing frontFacing = placer.getHorizontalFacing().getOpposite();
        buildFences(world, pos, frontFacing.getOpposite());
        return this.getDefaultState().withProperty(BlockHorizontal.FACING, frontFacing);
    }

    private void buildFences(World world, BlockPos pos, EnumFacing inner) {
        // Move to bottom left
        pos = pos.offset(inner, 1).offset(inner.rotateYCCW(), 2);

        EnumFacing direction = inner;
        for (int h = 0; h < 2; h++) { // Y coord
            for (int s = 0; s < 4; s++) { // Side
                for (int i = 0; i < 4; i++) { // X,Z coord
                    pos = pos.offset(direction);
                    world.setBlockState(pos, Blocks.OAK_FENCE.getDefaultState());
                }
                direction = direction.rotateY();
            }
            pos = pos.up();
        }

    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileMachine();
    }

    // Block State

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, BlockHorizontal.FACING);
    }

    @Override
    public IBlockState getStateFromMeta(int meta) {
        return this.getDefaultState().withProperty(BlockHorizontal.FACING, EnumFacing.getHorizontal(meta));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return state.getValue(BlockHorizontal.FACING).getHorizontalIndex();
    }

}
