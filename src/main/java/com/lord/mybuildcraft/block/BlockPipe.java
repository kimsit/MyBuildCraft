package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.tileentity.IPipeEntry;
import com.lord.mybuildcraft.tileentity.TileMachine;
import com.lord.mybuildcraft.tileentity.TilePipe;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.properties.PropertyBool;
import net.minecraft.block.state.BlockStateContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.IInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.AxisAlignedBB;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

import javax.annotation.Nullable;
import java.util.List;

public abstract class BlockPipe extends BlockBuildcraft {

    private static final PropertyBool SOUTH = PropertyBool.create("south");
    private static final PropertyBool NORTH = PropertyBool.create("north");
    private static final PropertyBool EAST = PropertyBool.create("east");
    private static final PropertyBool WEST = PropertyBool.create("west");
    private static final PropertyBool UP = PropertyBool.create("up");
    private static final PropertyBool DOWN = PropertyBool.create("down");

    private static final AxisAlignedBB center_AABB = new AxisAlignedBB(0.25D, 0.25D, 0.25D, 0.75D, 0.75D, 0.75D);
    private static final AxisAlignedBB south_AABB = new AxisAlignedBB(0.0D, 0.25D, 0.25D, 0.25D, 0.75D, 0.75D);
    private static final AxisAlignedBB north_AABB = new AxisAlignedBB(0.75D, 0.25D, 0.25D, 0.75D, 1.0D, 0.75D);
    private static final AxisAlignedBB west_AABB = new AxisAlignedBB(0.25D, 0.25D, 0.0D, 0.75D, 0.75D, 0.25D);
    private static final AxisAlignedBB east_AABB = new AxisAlignedBB(0.25D, 0.25D, 0.75D, 0.75D, 0.75D, 1.0D);
    private static final AxisAlignedBB up_AABB = new AxisAlignedBB(0.25D, 0.75D, 0.25D, 0.75D, 1.0D, 0.75D);
    private static final AxisAlignedBB down_AABB = new AxisAlignedBB(0.25D, 0.0D, 0.25D, 0.75D, 0.25D, 0.75D);

    public BlockPipe(String name, Material material) {
        super(name, material);
        setResistance(3F);
        setSoundType(SoundType.METAL);

        setDefaultState(this.blockState.getBaseState().withProperty(NORTH, false).withProperty(SOUTH, false)
                .withProperty(EAST, false).withProperty(WEST, false).withProperty(UP, false).withProperty(DOWN, false));
    }

    protected Boolean canConnectTo(IBlockAccess world, BlockPos pos) {
        TileEntity tile = world.getTileEntity(pos);
        return tile instanceof IPipeEntry || tile instanceof IInventory || tile instanceof TileMachine;
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
                                    EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            TileEntity tileEntity = worldIn.getTileEntity(pos);
            ItemStack heldCopy = playerIn.getHeldItem(hand).copy();
            heldCopy.setCount(1);

            if (tileEntity instanceof TilePipe) {
                ((TilePipe) tileEntity).injectItem(heldCopy, EnumFacing.NORTH);
            }
        }

        return true;
    }

    @Override
    public void addCollisionBoxToList(IBlockState state, World worldIn, BlockPos pos, AxisAlignedBB entityBox,
                                      List<AxisAlignedBB> collidingBoxes, @Nullable Entity entityIn, boolean p_185477_7_) {
        addCollisionBoxToList(pos, entityBox, collidingBoxes, center_AABB);

        if (state.getValue(NORTH)) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, north_AABB);
        }
        if (state.getValue(SOUTH)) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, south_AABB);
        }
        if (state.getValue(WEST)) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, west_AABB);
        }
        if (state.getValue(EAST)) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, east_AABB);
        }
        if (state.getValue(UP)) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, up_AABB);
        }
        if (state.getValue(DOWN)) {
            addCollisionBoxToList(pos, entityBox, collidingBoxes, down_AABB);
        }
    }

    public AxisAlignedBB getBoundingBox(IBlockState state, IBlockAccess source, BlockPos pos) {
        state = this.getActualState(state, source, pos);

        // TODO: proper bounding boxes
        return center_AABB;
    }

    @Override
    public BlockRenderLayer getBlockLayer() {
        // To render the block with transparency
        return BlockRenderLayer.CUTOUT;
    }

    @Override
    public boolean isOpaqueCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isFullCube(IBlockState state) {
        return false;
    }

    @Override
    public boolean isNormalCube(IBlockState state, IBlockAccess world, BlockPos pos) {
        return false;
    }

    @Override
    public boolean shouldSideBeRendered(IBlockState blockState, IBlockAccess blockAccess, BlockPos pos,
                                        EnumFacing side) {
        return true;
    }

    // Block State

    @Override
    protected BlockStateContainer createBlockState() {
        return new BlockStateContainer(this, NORTH, SOUTH, EAST, WEST, UP, DOWN);
    }

    @Override
    public IBlockState getActualState(IBlockState state, IBlockAccess worldIn, BlockPos pos) {
        return state.withProperty(NORTH, canConnectTo(worldIn, pos.north()))
                .withProperty(SOUTH, canConnectTo(worldIn, pos.south()))
                .withProperty(EAST, canConnectTo(worldIn, pos.east()))
                .withProperty(WEST, canConnectTo(worldIn, pos.west()))
                .withProperty(UP, canConnectTo(worldIn, pos.up()))
                .withProperty(DOWN, canConnectTo(worldIn, pos.down()));
    }

    @Override
    public int getMetaFromState(IBlockState state) {
        return 0;
    }

}
