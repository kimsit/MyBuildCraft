package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.Buildcraft;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;

public class BlockBuildcraft extends Block {

    public BlockBuildcraft(String name, Material material) {
        super(material);

        setUnlocalizedName(name);
        setRegistryName(Buildcraft.MODID, name);

        setCreativeTab(CreativeTabs.REDSTONE);
    }

    public Item createItemBlock() {
        return new ItemBlock(this).setRegistryName(getRegistryName());
    }
}
