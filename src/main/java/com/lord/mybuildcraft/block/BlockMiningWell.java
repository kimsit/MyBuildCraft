package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.tileentity.TileMiningWell;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockMiningWell extends BlockBuildcraft implements ITileEntityProvider {

    public BlockMiningWell(String name) {
        super(name, Material.IRON);
        setHardness(1.5F);
        setResistance(10F);
        setSoundType(SoundType.METAL);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
                                    EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            TileEntity tile = worldIn.getTileEntity(pos);

            if (tile instanceof TileMiningWell && !worldIn.isRemote) {
                ((TileMiningWell) tile).dig();
            }
        }

        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileMiningWell();
    }

}
