package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.Buildcraft;
import com.lord.mybuildcraft.GuiHandler;
import com.lord.mybuildcraft.tileentity.TileDiamondPipe;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

public class BlockDiamondPipe extends BlockPipe implements ITileEntityProvider {

    public BlockDiamondPipe(String name) {
        super(name, Material.IRON);
    }

    @Override
    public boolean onBlockActivated(World worldIn, BlockPos pos, IBlockState state, EntityPlayer playerIn,
                                    EnumHand hand, EnumFacing side, float hitX, float hitY, float hitZ) {
        if (!worldIn.isRemote) {
            playerIn.openGui(Buildcraft.INSTANCE, GuiHandler.GUI_ROOTER_ID, worldIn, pos.getX(), pos.getY(), pos.getZ());
        }
        return true;
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileDiamondPipe();
    }
}
