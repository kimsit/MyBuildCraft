package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.tileentity.TileStonePipe;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockStonePipe extends BlockPipe implements ITileEntityProvider {

    public BlockStonePipe(String name) {
        super(name, Material.ROCK);
    }

    @Nullable
    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileStonePipe();
    }
}
