package com.lord.mybuildcraft.block;

import com.lord.mybuildcraft.tileentity.TileGoldPipe;
import net.minecraft.block.ITileEntityProvider;
import net.minecraft.block.material.Material;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.world.World;

public class BlockGoldPipe extends BlockPipe implements ITileEntityProvider {

    public BlockGoldPipe(String name) {
        super(name, Material.IRON);
    }

    @Override
    public TileEntity createNewTileEntity(World worldIn, int meta) {
        return new TileGoldPipe();
    }
}
