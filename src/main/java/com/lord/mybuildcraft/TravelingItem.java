package com.lord.mybuildcraft;

import net.minecraft.entity.item.EntityItem;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.Vec3d;

public class TravelingItem {

    protected ItemStack stack;
    protected TileEntity container;

    private static int maxId = 0;

    public Vec3d pos;
    private int id;
    public boolean toCenter = true;
    public EnumFacing input = null;
    public EnumFacing output = null;

    protected float speed = 0.1F;

    private TravelingItem(int id) {
        this.pos = new Vec3d(0, 0, 0);
        this.id = id;
        this.stack = null;
    }

    public static TravelingItem make() {
        return new TravelingItem(maxId++);
    }

    public static TravelingItem make(Vec3d pos, ItemStack stack) {
        TravelingItem item = make();
        item.pos = pos;
        item.stack = stack.copy();
        return item;
    }

    public static TravelingItem fromNBT(NBTTagCompound compound) {
        TravelingItem item = make();
        item.readFromNBT(compound);
        return item;
    }

    public void move(Vec3d toAdd) {
        pos = pos.add(toAdd);
    }

    public EntityItem toEntityItem() {
        if (container != null && !container.getWorld().isRemote) {
            EntityItem entityitem = new EntityItem(container.getWorld(), pos.x, pos.y, pos.z, stack);
            entityitem.setDefaultPickupDelay();

            Vec3d motion = Utils.vecFromFacing(output, 0.1 + 2 * speed);

            float scale = container.getWorld().rand.nextFloat() * 0.04F - 0.02F;
            entityitem.motionX = motion.x + container.getWorld().rand.nextGaussian() * scale;
            entityitem.motionY = motion.y + container.getWorld().rand.nextGaussian() * scale;
            entityitem.motionZ = motion.z + container.getWorld().rand.nextGaussian() * scale;

            return entityitem;
        }
        return null;
    }

    public void readFromNBT(NBTTagCompound data) {
        id = data.getInteger("id");

        pos = new Vec3d(data.getDouble("x"), data.getDouble("y"), data.getDouble("z"));
        speed = data.getFloat("speed");

        NBTTagCompound itemStackTag = data.getCompoundTag("stack");
        stack = new ItemStack(itemStackTag);

        toCenter = data.getBoolean("toCenter");
        input = Utils.facingFromNBT(data.getTag("input"));
        output = Utils.facingFromNBT(data.getTag("output"));
    }

    public void writeToNBT(NBTTagCompound data) {
        data.setInteger("id", id);
        data.setDouble("x", pos.x);
        data.setDouble("y", pos.y);
        data.setDouble("z", pos.z);
        data.setFloat("speed", speed);

        NBTTagCompound itemStackTag = new NBTTagCompound();
        stack.writeToNBT(itemStackTag);
        data.setTag("stack", itemStackTag);

        data.setBoolean("toCenter", toCenter);
        data.setTag("input", Utils.NBTFromFacing(input));
        data.setTag("output", Utils.NBTFromFacing(output));
    }

    public ItemStack getStack() {
        return stack;
    }

    public void setStack(ItemStack stack) {
        this.stack = stack;
    }

    public TileEntity getContainer() {
        return container;
    }

    public void setContainer(TileEntity container) {
        this.container = container;
    }

    public float getSpeed() {
        return speed;
    }

    public void setSpeed(float speed) {
        this.speed = speed;
    }

    public int getId() {
        return id;
    }
}
