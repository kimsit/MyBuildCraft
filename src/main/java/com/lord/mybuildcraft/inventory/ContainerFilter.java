package com.lord.mybuildcraft.inventory;

import com.lord.mybuildcraft.tileentity.TileDiamondPipe;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.IInventory;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerFilter extends Container {

    private TileDiamondPipe rooter;
    public static final int ROWS = 6;
    public static final int COLUMNS = 8;
    public static final int SIZE = ROWS * COLUMNS;

    public ContainerFilter(IInventory playerInventory, TileDiamondPipe tileDiamondPipe) {
        this.rooter = tileDiamondPipe;

        IItemHandler inventory = rooter.getFilters();

        // Tile Entity, Slot 0-47, Slot IDs 0-47
        for (int y = 0; y < ROWS; ++y) {
            for (int x = 0; x < COLUMNS; ++x) {
                this.addSlotToContainer(new SlotItemHandler(inventory, x + y * COLUMNS, 26 + x * 18, 18 + y * 18) {
                    @Override
                    public void onSlotChanged() {
                        rooter.markDirty();
                    }
                });
            }
        }

        // Player Inventory, Slot 9-35, Slot IDs 48-74
        for (int y = 0; y < 3; ++y) {
            for (int x = 0; x < 9; ++x) {
                this.addSlotToContainer(new Slot(playerInventory, x + y * 9 + 9, 8 + x * 18, 140 + y * 18));
            }
        }

        // Player Inventory, Slot 0-8, Slot IDs 75-83
        for (int x = 0; x < 9; ++x) {
            this.addSlotToContainer(new Slot(playerInventory, x, 8 + x * 18, 198));
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int index) {
        ItemStack itemstack = ItemStack.EMPTY;
        Slot sourceSlot = inventorySlots.get(index);

        if (sourceSlot != null && sourceSlot.getHasStack()) {
            ItemStack itemstack1 = sourceSlot.getStack();
            itemstack = itemstack1.copy();

            int containerSlots = inventorySlots.size() - player.inventory.mainInventory.size();

            if (index < containerSlots) {
                // Merge in player inventory
                if (!this.mergeItemStack(itemstack1, containerSlots, inventorySlots.size(), true)) {
                    return ItemStack.EMPTY;
                }
            } else if (!this.mergeItemStack(itemstack1, 0, containerSlots, false)) {
                return ItemStack.EMPTY;
            }

            if (itemstack1.getCount() == 0) {
                sourceSlot.putStack(ItemStack.EMPTY);
            } else {
                sourceSlot.onSlotChanged();
            }

            if (itemstack1.getCount() == itemstack.getCount()) {
                return ItemStack.EMPTY;
            }

            sourceSlot.onTake(player, itemstack1);
        }

        return itemstack;
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }

}
