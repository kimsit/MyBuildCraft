package com.lord.mybuildcraft;

import com.lord.mybuildcraft.network.PipeUpdatePacket;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

@Mod(modid = Buildcraft.MODID, name = Buildcraft.NAME, version = Buildcraft.VERSION)
public class Buildcraft {
    public static final String MODID = "mybuildcraft";
    public static final String NAME = "My BuildCraft";
    public static final String VERSION = "0.1";

    @Instance(Buildcraft.MODID)
    public static Buildcraft INSTANCE;

    @SidedProxy(clientSide = "com.lord.mybuildcraft.client.ClientProxy", serverSide = "com.lord.mybuildcraft.CommonProxy")
    public static CommonProxy proxy;

    public static SimpleNetworkWrapper network;

    @EventHandler
    public void preInit(FMLPreInitializationEvent event) {
        proxy.preInit();

        network = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);
        network.registerMessage(PipeUpdatePacket.Handler.class, PipeUpdatePacket.class, 0, Side.CLIENT);
    }

    @EventHandler
    public void init(FMLInitializationEvent event) {
        proxy.init();
    }

    @EventHandler
    public void postInit(FMLPostInitializationEvent event) {
        proxy.postInit();
    }

}
