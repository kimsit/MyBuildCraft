#### MyBuildcraft

Toy project for learning purposes, inspired by the original [Buildcraft](https://github.com/BuildCraft/BuildCraft).

Playing around the Minecraft Forge modding APIs (mainly tile entities, GUIs, rendering hooks)

Features :
* item pipes carrying stuff around
* client server synchronization
* efficient block rendering with forge blockstates
* mining well tile

### Screenshots


Auto mining and piping :

![](https://user-images.githubusercontent.com/1765423/32671304-d9443bd8-c614-11e7-8cb6-79979495e4a9.png)

Mining well and pipe systems :

![](https://user-images.githubusercontent.com/1765423/32671324-edeb01de-c614-11e7-8fd3-3f2660ccfbcd.png)

Item routing :

![](https://user-images.githubusercontent.com/1765423/32671328-f1555fae-c614-11e7-85b7-0241f0dbd52a.png)